import Navbar from '../components/navbar';
import Footer from '../components/footer';
import '../scss/style.sass';

const Index = () => {
    return(
        <div className="index-main">
             <Navbar />
            <section>
            <div className='container'>
                <div className='philosophy'>
                    <h2>Nuestra filosofia</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque, mauris quis convallis eleifend, eros massa pretium velit, vitae ultricies velit dui sit amet magna. Nam a felis aliquam, maximus odio ut, lobortis mi. Suspendisse a ultrices augue, a malesuada nunc. Nullam convallis id erat et dictum. Nulla tristique sem in nunc tincidunt efficitur. Maecenas dignissim facilisis urna, malesuada bibendum velit lacinia vitae. Fusce viverra mollis ipsum ut ullamcorper. </p>
                </div>
                
                <div className='mission'>
                    <h2>Mision</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque, mauris quis convallis eleifend, eros massa pretium velit, vitae ultricies velit dui sit amet magna. Nam a felis aliquam, maximus odio ut, lobortis mi. Suspendisse a ultrices augue, a malesuada nunc. Nullam convallis id erat et dictum. Nulla tristique sem in nunc tincidunt efficitur. Maecenas dignissim facilisis urna, malesuada bibendum velit lacinia vitae. Fusce viverra mollis ipsum ut ullamcorper. </p>
                </div>

                <div className='vission'>
                    <h2>Vision</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque, mauris quis convallis eleifend, eros massa pretium velit, vitae ultricies velit dui sit amet magna. Nam a felis aliquam, maximus odio ut, lobortis mi. Suspendisse a ultrices augue, a malesuada nunc. Nullam convallis id erat et dictum. Nulla tristique sem in nunc tincidunt efficitur. Maecenas dignissim facilisis urna, malesuada bibendum velit lacinia vitae. Fusce viverra mollis ipsum ut ullamcorper. </p>    
                </div>
                <div className='vission'>
                    <h2>Objetivos</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque, mauris quis convallis eleifend, eros massa pretium velit, vitae ultricies velit dui sit amet magna. Nam a felis aliquam, maximus odio ut, lobortis mi. Suspendisse a ultrices augue, a malesuada nunc. Nullam convallis id erat et dictum. Nulla tristique sem in nunc tincidunt efficitur. Maecenas dignissim facilisis urna, malesuada bibendum velit lacinia vitae. Fusce viverra mollis ipsum ut ullamcorper. </p>    
                </div>
            </div>
        </section>
        <Footer/>
        </div>
    
    );
};

export default Index;