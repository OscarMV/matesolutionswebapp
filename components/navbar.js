import { Component } from "react";
import Link from "next/link";
import Header from "./head";

class Navbar extends Component {
  componentDidMount() {
    //Init the sidenav bar for mobile.
    var elems = document.querySelectorAll(".sidenav");
    const M = require("materialize-css");
    M.Sidenav.init(elems);
    M.AutoInit();
  }

  render() {
    return (
      <div className="navbar-fixed ">
        <Header />
        <div className="my-nav">
          <ul id="dropdown1" className="dropdown-content">
            <li>
              <a href="#!">BioNoether</a>
            </li>
            <li>
              <a href="#!">Advisapp</a>
            </li>
          </ul>
          <nav className="blue darken-4">
            <div className="nav-wrapper container ">
              <a href="#!" className="brand-logo logo-text">
                <span className="logo-img">&#5816;</span>
                Matesolutions
              </a>
              <a href="#" data-target="mobile-demo" className="sidenav-trigger">
                <i className="material-icons">menu</i>
              </a>
              <ul className="right hide-on-med-and-down">
                <li>
                  <a href="badges.html">Servicios</a>
                </li>
                <li>
                  <a className="dropdown-trigger" href="#!" data-target="dropdown1">
                    Proyectos
                    <i className="material-icons right">arrow_drop_down</i>
                  </a>
                </li>
              </ul>
            </div>
          </nav>
          <ul className="sidenav" id="mobile-demo">
            <li>
              <a >BioNoether</a>
              
            </li>
            <li>
               <a>Advisapp</a>
              
            </li>
            <li>
               <a>Servicios</a>
              
            </li>
          </ul>
        </div>
      </div>
    );
  }
}
export default Navbar;
